# The purpose of this code is the generate a set of variations for the Charm production fractions
# These variations can be used to reweight the existing MC samples to obtain a systematic
# uncertainty on the D+ mass distribution
import numpy as np
from scipy.linalg import eigh, cholesky, svd
from scipy.stats import norm

# Number of replicas to create the systematic band
num_samples = 1

# page 52: https://pdg.lbl.gov/2020/tables/rpp2020-tab-mesons-bottom.pdf
#        B+/B0,     Bs, Baryon
frac = [0.4080, 0.1000, 0.0840]

# sigmas
sigmas = np.diag([0.007, 0.008, 0.011])
print(sigmas)

# correlation
corr = np.zeros(3)
corr = np.array([[1     , -0.633, -0.813],
                 [-0.633, 1     , 0.064 ],
                 [-0.813, 0.064 , 1     ]])
print(corr)

# covariance
cov = np.dot(sigmas, np.dot(corr, sigmas))
print(cov)

# Generate samples from four independent normally distributed random
# variables (with mean 0 and std. dev. 1).  
# x = norm.rvs(size=(4, num_samples))
x = np.identity(3)

#
# Make an array with the offsets for the production fractions
offsets = [frac[0]*np.ones(num_samples),
           frac[1]*np.ones(num_samples),
           frac[2]*np.ones(num_samples)]

# We need a matrix `c` for which `c*c^T = cov`.
# We get this by computing  the eigenvalues and eigenvectors.

evals, evecs = eigh(cov)
print(evals)
# abs necessary below because one eigenvector is very close to zero and can be negative due to roundoff
# I've checked that the answer we get using svd is very close to this
c = np.dot(evecs, np.diag(np.sqrt(evals)))
# print (evals)
# print (evecs)

# Convert the data to correlated random variables.
y = np.dot(c, x) + offsets
print(y)

