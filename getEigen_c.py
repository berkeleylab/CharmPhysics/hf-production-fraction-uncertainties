# The purpose of this code is the generate a set of variations for the Charm production fractions
# These variations can be used to reweight the existing MC samples to obtain a systematic
# uncertainty on the D+ mass distribution
import numpy as np
from scipy.linalg import eigh, cholesky, svd
from scipy.stats import norm

# Number of replicas to create the systematic band
num_samples = 1

# These are the fractions from Table 12 of arXiv 1509.01061
# The order is: D+, D0, D_s, LambdaC.  Note:  The paper assumes that the total baryon rate is
#  0.136\pm 0.006 times the LambdaC rate.  We will ignore the small uncertainty on that fraction.
#  These central values are fine since 0.2404+0.6086+0.0802+1.135*0.0623 = 0.99991050
#  It is probably OK for us to not worry about varying the weights for the baryons other than LambdaC since
#  they only represent 0.8 per cent of the charmed hadrons.
frac = [0.2404,0.6086,0.0802,0.0623]
#
# This is the covariance matrix
# The numbers come from the correlation matrix in Table 13 of arXiv 1509.01061 multiplied by
# the sigmas from the middle column of Table 12
cov = np.array([
    [4.489e-05 ,-3.36072e-05 ,-5.092e-06 ,-5.2193e-06 ],
    [-3.36072e-05 ,5.776e-05 ,-9.728e-06 ,-1.27756e-05],
    [-5.092e-06 ,-9.728e-06 ,1.6e-05 ,-1.148e-06],
    [-5.2193e-06 ,-1.27756e-05 ,-1.148e-06 ,1.681e-05] ,
])


# Generate samples from four independent normally distributed random
# variables (with mean 0 and std. dev. 1).  
# x = norm.rvs(size=(4, num_samples))
x = np.identity(4)

#
# Make an array with the offsets for the production fractions
offsets = [frac[0]*np.ones(num_samples),
           frac[1]*np.ones(num_samples),
           frac[2]*np.ones(num_samples),
           frac[3]*np.ones(num_samples)]

# We need a matrix `c` for which `c*c^T = cov`.
# We get this by computing  the eigenvalues and eigenvectors.

evals, evecs = eigh(cov)
# abs necessary below because one eigenvector is very close to zero and can be negative due to roundoff
# I've checked that the answer we get using svd is very close to this
c = np.dot(evecs, np.diag(np.sqrt(abs(evals))))
# print (evals)
# print (evecs)

# Convert the data to correlated random variables.
y = np.dot(c, x) + offsets
print(y)

# Check that our generated values give the right means and sigmas
print("Dplus mean fraction: ",np.mean(y[0])," std: ",np.std(y[0]))
print("D0 mean fraction: ",np.mean(y[1])," std: ",np.std(y[1]))
print("Ds mean fraction: ",np.mean(y[2])," std: ",np.std(y[2]))
print("LambdaC mean: ",np.mean(y[3])," std: ",np.std(y[3]))
